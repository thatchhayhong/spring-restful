package com.hrd.practice.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class User {
    private int user_id;
    private String fullname;
    private String username;
    private String password;
}
