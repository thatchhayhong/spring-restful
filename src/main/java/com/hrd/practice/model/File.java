package com.hrd.practice.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class File {
    private String fileName;
    private long fileSize;
}
