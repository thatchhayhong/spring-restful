package com.hrd.practice.model;
import lombok.*;
@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserRole {
    private String name;
    private int role_id;
}
