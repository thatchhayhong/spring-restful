package com.hrd.practice.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Post {
    private int post_id;
    private String caption;
    private int image_id;
    private int user_id;
    private int totalLike;
}
