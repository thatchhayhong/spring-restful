package com.hrd.practice.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Comment {
    private int comment_id;
    private String comments;
    private int comment_parent;
    private int post_id;
    private int user_id;
}
