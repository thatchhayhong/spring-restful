package com.hrd.practice.Restcontroller;

import com.hrd.practice.model.File;
import com.hrd.practice.response.Response;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
@RestController
@RequestMapping("/api/v1/file")
public class FileUploadRestController {
    @PostMapping("/upload")
    public Response<File> uploadDB(@RequestParam("file")MultipartFile multipartFile){
        return Response.<File>ok().setPayload((File) multipartFile);
    }
}
