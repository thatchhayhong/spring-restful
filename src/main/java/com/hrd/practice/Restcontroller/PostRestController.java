package com.hrd.practice.Restcontroller;

import com.hrd.practice.model.Post;
import com.hrd.practice.model.User;
import com.hrd.practice.payload.PostDto;
import com.hrd.practice.payload.UserDto;
import com.hrd.practice.payload.mapper.PostMapper;
import com.hrd.practice.payload.mapper.UserMapper;
import com.hrd.practice.payload.request.PostRequest;
import com.hrd.practice.payload.request.UserRequest;
import com.hrd.practice.repository.PostRespository;
import com.hrd.practice.repository.UserRepository;
import com.hrd.practice.response.Response;
import com.hrd.practice.service.PostService;
import com.hrd.practice.service.UserService;
import com.hrd.practice.utils.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/post")
public class PostRestController {

  @Autowired
  private PostService postService;
  @Autowired
  private PostRespository postRespository;
  @Autowired
  private PostMapper postMapper;
    @GetMapping("/all")
    public Response<List<PostDto>> findAll() {
        List<PostDto> post = postService.findAll();
        if(post!=null){
            return Response.<List<PostDto>>ok().setPayload(post);
        }else{
            return Response.notFound();
        }
    }
    @PostMapping("/delete/{id}")
    public Response<PostDto> deleteUser(@PathVariable int id) {
        boolean isDeleted = postService.delete(id);
        if(isDeleted){
            return Response.<PostDto>ok();
        }else {
            return Response.notFound();
        }
    }



    @PutMapping("/update/{id}")
    public Response<PostDto> update(@PathVariable int id, @RequestBody PostRequest PostRequest){
        Post post = postMapper.postRequestToPost(PostRequest);
        boolean isUpdated= postService.update(id,post);
        PostDto postDto = postMapper.postToPostDto(post);
        if(isUpdated){
            return Response.<PostDto>ok();
        }else {
            return Response.notFound();
        }
    }

    @PostMapping("/create")
    public Response<PostDto> createUser(@RequestBody PostRequest postRequest) {
        boolean isCreated = postService.insert(postRequest);
        if (isCreated) {
            Post post = postMapper.postRequestToPost(postRequest);
            PostDto postDto = postMapper.postToPostDto(post);
            return Response.<PostDto>ok().setPayload(postDto);
        } else {
            return Response.<PostDto>badRequest().setErrors("Check your data again");
        }
    }
    @GetMapping("/paging")
    public Response<List<PostDto>> findByPaging(@RequestParam int page, @RequestParam int limit) {
        int totalUsers = postRespository.countAllUsers();
        Paging paging = new Paging();
        paging.setPage(page);
        paging.setLimit(limit);
        paging.setTotalCount(totalUsers);
        List<PostDto> posts = postRespository.findByPaging(paging);
        return Response.<List<PostDto>>ok().setPayload(posts).setMetadata(paging);
    }


}
