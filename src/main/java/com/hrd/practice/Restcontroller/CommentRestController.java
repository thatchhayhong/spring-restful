package com.hrd.practice.Restcontroller;

import com.hrd.practice.model.Comment;
import com.hrd.practice.model.User;
import com.hrd.practice.payload.CommentDto;
import com.hrd.practice.payload.UserDto;
import com.hrd.practice.payload.mapper.CommentMapper;
import com.hrd.practice.payload.request.CommentRequest;
import com.hrd.practice.payload.request.UserRequest;
import com.hrd.practice.response.Response;
import com.hrd.practice.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
@RestController
@RequestMapping("/api/v1/comment")
public class CommentRestController {
    @Autowired
    private CommentService commentService;
    @Autowired
    private CommentMapper commentMapper;
    @GetMapping("/find/{id}")
    public Response<CommentDto> findUserById(@PathVariable int id) {
        CommentDto commentDto = commentService.findOne(id);
        if (commentDto == null) {
            return Response
                    .<CommentDto>notFound()
                    .setErrors("Comment with id " + id + " not found");
        }
        return Response.<CommentDto>ok().setPayload(commentDto);
    }

    @PostMapping("/delete/{id}")
    public Response<CommentDto> deleteComment(@PathVariable int id) {
        boolean isDeleted = commentService.delete(id);
        if(isDeleted){
            return Response.<CommentDto>ok();
        }else {
            return Response.notFound();
        }
    }

    @PutMapping("/update/{id}")
    public Response<CommentDto> updateComment(@PathVariable int id, @RequestBody CommentRequest commentRequest){
        Comment comment = commentMapper.CommentRequestToComment(commentRequest);
        boolean isUpdated= commentService.update(id,comment);
        CommentDto commentDto = commentMapper.CommentToCommentDto(comment);
        if(isUpdated){
            return Response.<CommentDto>ok();
        }else {
            return Response.notFound();
        }
    }
    @PostMapping("/create")
    public Response<CommentDto> createComment(@RequestBody CommentRequest commentRequest) {
        boolean isCreated = commentService.insert(commentRequest);
        if (isCreated) {
            Comment comment = commentMapper.CommentRequestToComment(commentRequest);
            CommentDto commentDto = commentMapper.CommentToCommentDto(comment);
            return Response.<CommentDto>ok().setPayload(commentDto);
        } else {
            return Response.<CommentDto>badRequest().setErrors("Check your data again");
        }
    }

}
