package com.hrd.practice.Restcontroller;

import com.hrd.practice.model.User;
import com.hrd.practice.model.UserRole;
import com.hrd.practice.payload.UserDto;
import com.hrd.practice.payload.mapper.UserMapper;
import com.hrd.practice.payload.request.UserRequest;
import com.hrd.practice.repository.UserRepository;
import com.hrd.practice.response.Response;
import com.hrd.practice.service.UserService;
import com.hrd.practice.utils.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/users")
public class UserRestController {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserService userService;
//    @GetMapping("/all")
//    public Response<List<UserDto>> findAll() {
//        List<UserDto> users = userService.findAll();
//        return Response.<List<UserDto>>ok().setPayload(users);
//    }
    @GetMapping("/find/{id}")
    public Response<UserDto> findUserById(@PathVariable int id) {
        UserDto user = userService.findOne(id);
        List<String> user_role = userRepository.findUserRole(id);
        if (user == null) {
            return Response
                    .<UserDto>notFound()
                    .setErrors("User with id " + id + " not found");
        }
        user.setUser_role(user_role);
        return Response.<UserDto>ok().setPayload(user);
    }
    @PostMapping("/delete/{id}")
    public Response<UserDto> deleteUser(@PathVariable int id) {
        boolean isDeleted = userService.delete(id);
      if(isDeleted){
          return Response.<UserDto>ok();
      }else {
          return Response.notFound();
      }
    }

//    @GetMapping("/paging")
//    public Response<List<UserDto>> findByPaging(@RequestParam int page, @RequestParam int limit) {
//        int totalUsers = userRepository.countAllUsers();
//        Paging paging = new Paging();
//        paging.setPage(page);
//        paging.setLimit(limit);
//        paging.setTotalCount(totalUsers);
//        List<UserDto> users = userRepository.findByPaging(paging);
//        return Response.<List<UserDto>>ok().setPayload(users).setMetadata(paging);
//    }


    @PutMapping("/update/{id}")
    public Response<UserDto> update(@PathVariable int id, @RequestBody UserRequest userRequest){
        User user = userMapper.userRequestToUser(userRequest);
        boolean isUpdated= userService.update(id,user);
        UserDto userDto = userMapper.userToUserDto(user);
        if(isUpdated){
            return Response.<UserDto>ok();
        }else {
            return Response.notFound();
        }
    }
    @PostMapping("/create")
    public Response<UserDto> createUser(@RequestBody UserRequest userRequest) {
        System.out.println(userRequest);
        boolean isCreated = userService.insert(userRequest);
        if (isCreated) {
             User user = userMapper.userRequestToUser(userRequest);
             UserDto userdto = userMapper.userToUserDto(user);
            return Response.<UserDto>ok().setPayload(userdto);
        } else {
            return Response.<UserDto>badRequest().setErrors("Check your data again");
        }
    }

}
