package com.hrd.practice.payload.mapper;
import com.hrd.practice.model.User;
import com.hrd.practice.payload.UserDto;
import com.hrd.practice.payload.request.UserRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {

    User userDtoToUser(UserRequest userDto);

//    @Mapping(source = "user_id", target = "id")
    UserDto userToUserDto(User user);

    User userRequestToUser(UserRequest userRequest);

//    List<User> userRequestsToUsers(List<UserRequest> userRequest);
//    default String mapIntIdToString(int id) {
//        return "" + id;
//    }

}
