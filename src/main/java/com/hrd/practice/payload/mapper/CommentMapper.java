package com.hrd.practice.payload.mapper;

import com.hrd.practice.model.Comment;
import com.hrd.practice.payload.CommentDto;
import com.hrd.practice.payload.request.CommentRequest;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CommentMapper {
    Comment CommentDtoToComment(CommentRequest CommentRequest);
    //    @Mapping(source = "user_id", target = "id")
    CommentDto CommentToCommentDto(Comment comment);

    Comment CommentRequestToComment(CommentRequest CommentRequest);

//    List<User> userRequestsToUsers(List<UserRequest> userRequest);
//    default String mapIntIdToString(int id) {
//        return "" + id;
//    }




}
