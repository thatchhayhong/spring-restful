package com.hrd.practice.payload.mapper;

import com.hrd.practice.model.Post;
import com.hrd.practice.payload.PostDto;
import com.hrd.practice.payload.request.PostRequest;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PostMapper {
    Post postDtoToPost(PostRequest postRequest);

    //    @Mapping(source = "user_id", target = "id")
    PostDto postToPostDto(Post post);

    Post postRequestToPost(PostRequest postRequest);
}
