package com.hrd.practice.payload;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PostDto {
    private int post_id;
    private String caption;
    private int image_id;
    private int user_id;
    private int totalLike;
}
