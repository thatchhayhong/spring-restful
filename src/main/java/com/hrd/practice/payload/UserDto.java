package com.hrd.practice.payload;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hrd.practice.model.UserRole;
import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
public class UserDto {
    private String user_id;
    private String fullname;
    private String username;
    private String password;
    private List<String> user_role;
}
