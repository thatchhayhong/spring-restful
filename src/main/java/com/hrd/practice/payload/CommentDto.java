package com.hrd.practice.payload;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CommentDto {
    private int comment_id;
    private String comments;
    private int comment_parent;
    private int post_id;
    private int user_id;
}
