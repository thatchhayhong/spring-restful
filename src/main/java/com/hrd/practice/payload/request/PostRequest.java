package com.hrd.practice.payload.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PostRequest {
    private String caption;
    private int image_id;
    private int user_id;
}
