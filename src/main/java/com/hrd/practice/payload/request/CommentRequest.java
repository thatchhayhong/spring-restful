package com.hrd.practice.payload.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CommentRequest {
    private String comments;
    private int post_id;
    private int user_id;
}
