package com.hrd.practice.payload.request;

import com.hrd.practice.model.UserRole;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class UserRequest {
    private String fullname;
    private String username;
    private String password;
    private List<String> user_role;
}