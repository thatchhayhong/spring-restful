package com.hrd.practice.service;
import com.hrd.practice.model.Comment;
import com.hrd.practice.payload.CommentDto;
import com.hrd.practice.payload.request.CommentRequest;


public interface CommentService {
    public CommentDto findOne(int id);
    public boolean update(int id, Comment comment);
    public boolean delete(int id);
    public boolean insert(CommentRequest user);

}
