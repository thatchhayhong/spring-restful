package com.hrd.practice.service;

import com.hrd.practice.model.User;
import com.hrd.practice.payload.UserDto;
import com.hrd.practice.payload.request.UserRequest;

import java.util.List;

public interface UserService {
    public List<UserDto> findAll();
    public UserDto findOne(int id);
    public boolean update(int id, User user);
    public boolean delete(int id);
    public boolean insert(UserRequest user);

}
