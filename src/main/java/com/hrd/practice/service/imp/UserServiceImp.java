package com.hrd.practice.service.imp;

import com.hrd.practice.model.User;
import com.hrd.practice.payload.UserDto;
import com.hrd.practice.payload.mapper.UserMapper;
import com.hrd.practice.payload.request.UserRequest;
import com.hrd.practice.repository.UserRepository;
import com.hrd.practice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class UserServiceImp  implements UserService {
    private UserRepository userRepository;
    private UserMapper userMapper;
    @Autowired
    public UserServiceImp(UserRepository userRepository, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    @Override
    public List<UserDto> findAll() {
        return userRepository.findAll();
    }

    @Override
    public UserDto findOne(int id) {
        return userRepository.findUserById(id);
    }

    @Override
    public boolean update(int id, User user) {
        return userRepository.update(id,user);
    }

    @Override
    public boolean delete(int id) {
        return userRepository.deleteUser(id);
    }

    @Override
    public boolean insert(UserRequest userRequest) {
        User user = userMapper.userDtoToUser(userRequest);
       return userRepository.createUser(user);
    }
}
