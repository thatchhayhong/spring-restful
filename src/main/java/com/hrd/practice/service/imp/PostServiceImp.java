package com.hrd.practice.service.imp;

import com.hrd.practice.model.Post;
import com.hrd.practice.payload.PostDto;
import com.hrd.practice.payload.mapper.PostMapper;
import com.hrd.practice.payload.request.PostRequest;
import com.hrd.practice.repository.PostRespository;
import com.hrd.practice.service.PostService;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class PostServiceImp implements PostService {
    private PostRespository postRespository;
    private PostMapper postMapper;

    public PostServiceImp(PostRespository postRespository, PostMapper postMapper) {
        this.postRespository = postRespository;
        this.postMapper = postMapper;
    }

    @Override
    public List<PostDto> findAll() {
        return postRespository.findAll();
    }

    @Override
    public PostDto findOne(int id) {
        return postRespository.findPostById(id);
    }

    @Override
    public boolean update(int id, Post post) {
        return postRespository.update(id,post);
    }

    @Override
    public boolean delete(int id) {
        return postRespository.deletePost(id);
    }

    @Override
    public boolean insert(PostRequest postRequest) {
        Post post = postMapper.postDtoToPost(postRequest);
        System.out.println(post);
        return postRespository.createPost(post);
    }

}
