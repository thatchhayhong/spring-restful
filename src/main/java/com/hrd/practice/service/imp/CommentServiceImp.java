package com.hrd.practice.service.imp;

import com.hrd.practice.model.Comment;
import com.hrd.practice.payload.CommentDto;
import com.hrd.practice.payload.mapper.CommentMapper;
import com.hrd.practice.payload.request.CommentRequest;
import com.hrd.practice.repository.CommentRespository;
import com.hrd.practice.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommentServiceImp implements CommentService {
    @Autowired
    private CommentRespository commentRespository;
    @Autowired
    private CommentMapper commentMapper;

    @Override
    public CommentDto findOne(int id) {
        return commentRespository.findCommentById(id);
    }

    @Override
    public boolean update(int id, Comment comment) {
        return commentRespository.update(id,comment);
    }

    @Override
    public boolean delete(int id) {
        return commentRespository.deleteComment(id);
    }

    @Override
    public boolean insert(CommentRequest commentRequest) {
        Comment comment = commentMapper.CommentDtoToComment(commentRequest);
        System.out.println(comment);
        return commentRespository.createComment(comment);
    }
}
