package com.hrd.practice.service;

import com.hrd.practice.model.Post;
import com.hrd.practice.model.User;
import com.hrd.practice.payload.PostDto;
import com.hrd.practice.payload.UserDto;
import com.hrd.practice.payload.request.PostRequest;
import com.hrd.practice.payload.request.UserRequest;

import java.util.List;

public interface PostService {
    public List<PostDto> findAll();
    public PostDto findOne(int id);
    public boolean update(int id, Post post);
    public boolean delete(int id);
    public boolean insert(PostRequest post);
}
