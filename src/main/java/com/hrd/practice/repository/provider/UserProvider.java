package com.hrd.practice.repository.provider;

import com.hrd.practice.model.User;
import org.apache.ibatis.jdbc.SQL;

public class UserProvider {
    public String update(int id, User user){
        return new SQL(){{
            UPDATE("users u");
            SET("username=#{user.username},fullname=#{user.fullname},password=#{user.password}");
            WHERE("u.user_id=#{id}");
        }}.toString();
    }

}
