package com.hrd.practice.repository.provider;

import com.hrd.practice.model.Comment;
import org.apache.ibatis.jdbc.SQL;

public class CommentProvider {
    public String insert(Comment comment){
        return new SQL(){{
            INSERT_INTO("comments");
            VALUES("comments,post_id,user_id","#{comment.comments},#{comment.post_id},#{comment.user_id}");
        }}.toString();
    }
    public String update(int id, Comment comment){
        return new SQL(){{
            UPDATE("comments c");
            SET("comments=#{comment.comments},post_id=#{comment.post_id},user_id=#{comment.user_id}");
            WHERE("c.comment_id=#{id}");
        }}.toString();
    }
}
