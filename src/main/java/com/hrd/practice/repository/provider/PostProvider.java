package com.hrd.practice.repository.provider;

import com.hrd.practice.model.Post;
import org.apache.ibatis.jdbc.SQL;

public class PostProvider {
    public String insert(Post post){
        return new SQL(){{
            INSERT_INTO("posts");
            VALUES("caption,image_id,user_id","#{post.caption},#{post.image_id},#{post.user_id}");
        }}.toString();
    }
    public String update(int id, Post post){
        return new SQL(){{
            UPDATE("posts a");
            SET("caption=#{post.caption},image_id=#{post.image_id},user_id=#{post.user_id}");
            WHERE("a.post_id=#{id}");
        }}.toString();
    }
}
