package com.hrd.practice.repository;

import com.hrd.practice.model.Comment;
import com.hrd.practice.payload.CommentDto;
import com.hrd.practice.repository.provider.CommentProvider;
import org.apache.ibatis.annotations.*;
 @Mapper
    public interface CommentRespository {
        @Select("select * from comments where comment_id=#{id}")
        CommentDto findCommentById(int id);

//        @InsertProvider(type = CommentProvider.class,method = "insert")
        @Insert("insert into comments (comments,post_id,user_id) values (#{comments},#{post_id},#{user_id})")
//        @Insert("insert into users (fullname,username, password) values (#{fullname},#{username}, #{password})")
        boolean createComment(Comment comment);

        @UpdateProvider(type = CommentProvider.class,method = "update")
        public Boolean update(int id,@Param("comment") Comment comment);

        @Delete("DELETE FROM comments WHERE comment_id=#{id}")
        boolean deleteComment(int id);

}
