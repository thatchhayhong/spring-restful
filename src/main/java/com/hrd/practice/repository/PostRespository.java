package com.hrd.practice.repository;
import com.hrd.practice.model.Post;
import com.hrd.practice.model.User;
import com.hrd.practice.payload.PostDto;
import com.hrd.practice.payload.UserDto;
import com.hrd.practice.repository.provider.PostProvider;
import com.hrd.practice.utils.Paging;
import org.apache.ibatis.annotations.*;

import java.util.List;
    @Mapper
    public interface PostRespository {
//        @Select("SELECT * from user_role INNER JOIN users on user_role.user_id = users.user_id INNER JOIN roles on user_role.role_id= roles.role_id")
//        @SelectProvider(type = PostProvider.class,method = "findAll")
        @Select("select * from posts")
        @Results(id = "postMapping",
                value = {
                        @Result(column = "id",property = "posts.post_id"),
                })
        List<PostDto> findAll();

        @Select("select * from posts where post_id=#{id}")
        @ResultMap("postMapping")
        PostDto findPostById(int id);


        @InsertProvider(type = PostProvider.class,method = "insert")
        boolean createPost(@Param("post") Post post);

        @UpdateProvider(type = PostProvider.class,method = "update")
        public Boolean update(int id,@Param("post") Post post);


        @Insert("DELETE FROM posts WHERE post_id=#{id}")
//        @DeleteProvider(type = PostProvider.class,method = "delete")
        boolean deletePost(int id);

        @Select("SELECT * from user_role INNER JOIN users on user_role.user_id = users.user_id INNER JOIN roles on user_role.role_id= roles.role_id limit #{limit} offset #{offset}")
        @ResultMap("postMapping")
        List<PostDto> findByPaging(Paging paging);
        @Select("select count(user_id) from users")
        int countAllUsers();

    }