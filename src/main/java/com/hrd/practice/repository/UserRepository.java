package com.hrd.practice.repository;

import com.hrd.practice.model.User;
import com.hrd.practice.payload.UserDto;
import com.hrd.practice.repository.provider.UserProvider;
import com.hrd.practice.utils.Paging;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface UserRepository {
    @Select("SELECT * from user_role INNER JOIN users on user_role.user_id = users.user_id INNER JOIN roles on user_role.role_id= roles.role_id")
    @Results(id = "userMapping",
            value = {
                    @Result(column = "role_id",property = "user_role.role_id"),
                    @Result(column = "name",property = "user_role.name"),
            })
    List<UserDto> findAll();
//    @Select("SELECT * from user_role INNER JOIN users on user_role.user_id = users.user_id INNER JOIN roles on user_role.role_id= roles.role_id where users.user_id = #{id}")
    @Select("select * from users WHERE user_id=#{id}")
    @ResultMap("userMapping")
    UserDto findUserById(int id);

    @Select("select rt.name from user_role as urt inner join roles rt on urt.role_id = rt.role_id inner join  users ut on urt.user_id = ut.user_id where ut.user_id=46")
    List<String> findUserRole(int id);


    @Insert("insert into users (fullname,username, password) values (#{fullname},#{username}, #{password})")
    boolean createUser(User user);

    @UpdateProvider(type = UserProvider.class,method = "update")
    public Boolean update(int id,@Param("user") User user);

    @Insert("DELETE FROM users WHERE user_id=#{id}")
    boolean deleteUser(int id);
    @Select("SELECT * from user_role INNER JOIN users on user_role.user_id = users.user_id INNER JOIN roles on user_role.role_id= roles.role_id limit #{limit} offset #{offset}")
    @ResultMap("userMapping")
    List<UserDto> findByPaging(Paging paging);
    @Select("select count(user_id) from users")
    int countAllUsers();

}
